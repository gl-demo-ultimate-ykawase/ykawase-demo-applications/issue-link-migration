# This is a migration project from self-managed gitlab instance to GitLab.com
# this script exports all the issues and linked issue information to import to GitLab.com
# 1. authenticate with self-managed gitlab instance
# 2. input project id as a variable
# 3. list all the issues for the project
# 4. query linked issues for each issue
# 5. output issue id and its linked items to a file

# Export all issues and linked issues to a file
import gitlab
import os

# Connect to the self-managed GitLab instance
gl = gitlab.Gitlab('https://gitlab.com', private_token='glpat-4zHMFiNzrZuhekcjy5_V')

# Get the project ID
project_id = 46925769

# Get the project
project = gl.projects.get(project_id)

# Get all the issues for the project
issues = project.issues.list(all=True)

# Open a file to write the issue and linked issue data
with open('issue_export.csv', 'w') as f:
    f.write('Issue ID,Linked Issues\n')
    for issue in issues:
        # Get the linked issues for the current issue
        linked_issues = issue.links.list()
        linked_issue_ids = [str(linked_issue.iid) for linked_issue in linked_issues]
        f.write(f"{issue.iid},{','.join(linked_issue_ids)}\n")

